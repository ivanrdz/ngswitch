import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ngswitchcase';
  datos = [
  	{id:0, nombre:'ivan', edad:25},
  	{id:1, nombre:'jose', edad:26},
  	{id:2, nombre:'max', edad:27},
  	{id:3, nombre:'daniel', edad:28},
  	{id:4, nombre:'maria', edad:29},
  	{id:5, nombre:'jesse', edad:30},
  	{id:6, nombre:'oziel', edad:25},
  	{id:7, nombre:'raquel', edad:25},
  ];
  switchtest:any;

  fn(dato) {
  	console.log(dato);
  	if (dato === 'activo') { 
  		return this.switchtest = dato;
  	}
  	if (dato === 'pendiente') { 
  		return this.switchtest = dato;
  	}
  	if (dato === 'cancelado') { 
  		return this.switchtest = dato;
  	}
  }
}
